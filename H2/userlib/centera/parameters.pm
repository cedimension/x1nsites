
package U::Centera::Parameters;

use strict;

#use XReport::AUTOLOAD;
use Data::Dumper ();
use File::Basename ();

my $peafiles = File::Basename::dirname(__FILE__) . "/peafiles";

my %profiles_c1 = (
);

my %profiles_c2 = (
  '00400' => ['Zivnostenska Banka', 'E5', 'E5XREPORT'],
  '02008' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  'ISAIN' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  'EMOC0' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  'CM020' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  'CG020' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  '02700' => ['UniCredit Bank - CZ', 'E5', 'E5XREPORT'],
  '03131' => ['UniCredit Banca Mobiliare', 'AG', 'AGXREPORT'],
  '03135' => ['UniCredito Italiano Holding', 'UI', 'UIXREPORT'],
  '03198' => ['UniCredit Banca per la Casa', 'SQ', 'SQXREPORT'],
  '03214' => ['UniCredit Xelion Banca', 'UT', 'UTXREPORT'],
  '03218' => ['Clarima', 'UR', 'URXREPORT'],
  '03223' => ['UniCredit Private Banking', 'U6', 'U6XREPORT'],
  '03226' => ['UniCredit Banca d\'Impresa', 'U3', 'U3XREPORT'],
  '03307' => ['S2 Banca S.p.A.', 'S2', 'S2XREPORT'],
  '06095' => ['Cassa di Risparmio di Bra', 'E0', 'E0XREPORT'],
  '06105' => ['Cassa di Risparmio di Carpi', 'RC', 'RCXREPORT'],
  '06170' => ['Cassa di Risparmio di Fossano', 'F0', 'F0XREPORT'],
  '06235' => ['Banca dell\'Umbria 1462', 'RU', 'RUXREPORT'],
  '06285' => ['Cassa di Risparmio di Rimini', 'S7', 'S7XREPORT'],
  '06295' => ['Cassa di Risparmio di Saluzzo', 'G0', 'G0XREPORT'],
  '06305' => ['Cassa di Risparmio di Savigliano', 'H0', 'H0XREPORT'],
  '10639' => ['UGC Banca', 'GC', 'GCXREPORT'],
  '10639' => ['Medio Venezie', 'S1', 'GCXREPORT'],
  '16369' => ['Pioneer Investments', 'A2', 'A2XREPORT'],
  '80415' => ['Clarima Germania FBE', 'CD', 'CDXREPORT'],
  '99996' => ['Cartolarizzazioni Banca per la Casa', 'CQ', 'CQXREPORT'],
  'EMOCQ' => ['Cartolarizzazioni Banca per la Casa', 'CQ', 'CQXREPORT'],
  '99998' => ['Cartolarizzazioni Banca per la Casa', 'CE', 'CEXREPORT'],
  'EMOCE' => ['Cartolarizzazioni Banca per la Casa', 'CE', 'S0XREPORT'],
  'EMOBW' => ['Cartolarizzazioni Banca per la Casa', 'BW', 'S0XREPORT'],
  '06320' => ['Cassa di Risparmio di Torino', 'A0', 'A0XREPORT'],
  '05512' => ['Banca Popolare di Cremona', 'S5', 'S5XREPORT'],
  '03161' => ['TradingLab', 'TL', 'TLXREPORT'],
  '05437' => ['Bipop Carire', 'BZ', 'BZXREPORT'],
  '01020' => ['Banco di Sicilia', 'BD', 'BDXREPORT'],
  '03002' => ['Banca di Roma', 'BR', 'BRXREPORT'],
  'XXXXX' => ['MultiBanca', 'X1', 'X1XREPORT'],
  'Xena'  => ['MultiBanca', 'US', 'USXena'],
);

my %pools = (
  100001 => ['03198', 'NPU'], #UnicreditNPA
  100101 => ['03198', 'NPU'], #UnicreditNPU
  100201 => ['03198', 'NPU'], #UnicreditAdalya
  200001 => ['02008', 'DOCCLI50'], #XFORM
  400001 => ['03214', 'SAX'], #XelionSax
  400101 => ['03223', 'SAD6'], #SAD6
  500101 => ['02008', 'ARNXX'], #ARNCM
  500201 => ['80415', 'ARNXX'], #ARNCD
  500103 => ['02008', 'ARNXX'], #ARNCMFAXP
  500203 => ['02008', 'ARNXX'], #ARNCMFAXC  
  500104 => ['03135', 'GSG'], #GSG 
  500204 => ['03135', 'GSG'], #GSGCOLL
  600001 => ['XXXXX', 'GAR'], #UnicreditGAR
  600002 => ['XXXXX', 'GAR'], #UnicreditGARLET
  700001 => ['03226', 'FDBON'], #UBIFdBon
  700002 => ['03214', 'FDBON'], #XelionFdBon
  700003 => ['03307', 'FDBON'], #2SBancaFdBon
  700004 => ['02008', 'FDBON'], #RetailFdBon
  700005 => ['05437', 'FDBON'], #BZFdBon
  700006 => ['03002', 'FDBON'], #BRFdBon
  700007 => ['01020', 'FDBON'], #BDFdBon
  700008 => ['02008', 'GSG'], #BIN
  700009 => ['02008', 'GSG'], #BancaAss
  999991 => ['03002', 'FDBON'], #BRFdBonTest
);

my $lips = join(", ", map {"uscasl00$_.intranet.unicredit.it"} (1..4), map {"uscasl30$_.intranet.unicredit.it"} (1..4));

my %centeras = (
  c1 => ["10.182.32.117, 10.182.32.118, 10.182.32.119, 10.182.32.120", \%profiles_c1],
  c2 => [join(", ", map {"uscasl30$_.intranet.unicredit.it"} (1..4)), \%profiles_c2]
);

#my %centeras = (
#  c1 => ["10.182.32.117, 10.182.32.118, 10.182.32.119, 10.182.32.120", \%profiles_c1],
#  c2 => ["10.248.42.201,10.248.42.202,10.248.42.203,10.248.42.204", \%profiles_c2]
#);

sub AccessNodes {
  my $self = shift; return $self->{access_nodes};
}

sub ProfileFileName {
  my ($self, $profile) = @_; my $profiles_table = $self->{'profiles_table'};

  if (!exists($profiles_table->{$profile})) {
    my $msg = "profilo centera $profile non in tabella";
    i::logit($msg);
    die $msg, "\n";
  }
  $profile = $profiles_table->{$profile}->[2]; return "$peafiles/ALL_${profile}.pea";
}

my %XenaStorageClasses = (
  540 => 'USXena540d', 3650 => 'USXena10', 7300 => 'USXena20', 18250 => 'USXena50',
);

sub XenaParameters {
  my ($self, $io, $largs_file) = @_; 
  my ( $retention_period, $identify ) = $largs_file->getValues(qw(retention_period identify));

  my $storage_class = $XenaStorageClasses{$retention_period} 
   or do { my $msg = "Xena retention_period $retention_period not in defined table."; i::logit($msg); die $msg, "\n"; };
  
  my $largs_account = [ banca => 'Xena', doc_class => "file" ];

  my $largs_centera = { 
  	profile => 'Xena', 
    identify => $identify,
    account => $largs_account,
    storage_class => $storage_class, 
  };
  $main::veryverbose && i::logit("FileParameters: ", Data::Dumper::Dumper($largs_centera));
  return $largs_centera;
}

sub FileParameters {
  my ($self, $io, $largs_file) = @_; 
  my $profiles_table = $self->{'profiles_table'}; 

  my ( $JobReportName, $JobReportId, $LocalFileName ) = $largs_file->getValues(qw(JobReportName JobReportId LocalFileName)); 
  
  my $banca = substr($JobReportName, 1,5);
  if (exists($profiles_table->{$banca})) {
  }
  elsif ($JobReportName =~ /^..00EMU6/) {
    $banca = '03223';
  }
  elsif ($JobReportName =~ /^..00EMC0/) {
    $banca = '02008';
  }
  elsif ($JobReportName eq "RENDPIONEER") {
    $banca = '16369';
  }
#  elsif ($JobReportName eq "LC00EMTO0001" ) {
#    my $dbr = dbExecute("SELECT * from u0xrindex.dbo.tbl_IDX_TORINO_CONTI_CORRENTI where JobReportId=$JobReportId");
#    $banca = substr("00000".$dbr->getValues('ABI'),-5);
#    $dbr->Close();
#  }
#  elsif ($JobReportName eq "LC00EMTO0002" ) {
#    my $dbr = dbExecute("SELECT * from u0xrindex.dbo.tbl_IDX_TORINO_SCALARI where JobReportId=$JobReportId");
#    $banca = substr("00000".$dbr->getValues('ABI'),-5);
#    $dbr->Close();
#  }
  elsif ($JobReportName =~ /^..00EMBL/) {
    $banca = '02008';
  }
  elsif ($JobReportName =~ /^USXena/) {
    return $self->XenaParameters($io, $largs_file);
  }
  else {
    my $msg = "banca(profilo) $banca non in tabella per $JobReportName";
    return $self->XenaParameters($io, $largs_file);
    i::logit($msg); die $msg, "\n";
  }
  
  my $profile = $banca;
  my $storage_class = "DOCCLI";
  my $retention_period = 0;
  my $largs_account = [ banca => $banca, doc_class => "file" ];
  
  my $largs_centera = {
    profile => $profile, 
    account => $largs_account,
    storage_class => $storage_class, 
    retention_period => $retention_period, 
  };

  $main::veryverbose && i::logit("FileParameters: ", Data::Dumper::Dumper($largs_centera));
  return $largs_centera;
}

sub ObjectParameters {
  my ($self, $io, %largs_object) = @_; 

  my (
    $PoolId, $ObjectId, $ObjectRef, $ObjectType
  ) =
  @largs_object{qw(PoolId ObjectId ObjectRef ObjectType)};

  die("POOLID $PoolId NOT CONFIGURED !!!")
   if 
  !exists($pools{$PoolId});

  my ($profile, $storage_class) = @{$pools{$PoolId}};

  my $retention_period = 0;
  my $largs_account = [
    banca => $profile, doc_class => "image"
  ];

  my $largs_centera = {
    profile => $profile, 
    account => $largs_account,
    storage_class => $storage_class, 
    retention_period => $retention_period, 
  };

  return $largs_centera;
}
#identify => [pippo => 'pluto', paperino => "paperina"],

sub new {
  $main::veryverbose && i::logit( "new Parameters called by ", join('::', (caller())[0,2]), " args: ", join('::', @_));
  my ($class, $centera_id) = @_; $centera_id = lc($centera_id);

  my ($access_nodes, $profiles_table) = @{$centeras{$centera_id}};

  my $self = { 
    centera_id => $centera_id, 
    access_nodes => $access_nodes, 
    profiles_table => $profiles_table 
  };
  
  bless $self, $class;
}

__PACKAGE__;
