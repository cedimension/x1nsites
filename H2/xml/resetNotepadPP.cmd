::@echo off
@setlocal enableextensions enabledelayedexpansion
::for /F "usebackq tokens=1,2,3,4,5,6,7 delims=-//.:, " %a IN (`echo %date%-%time%`) do set my_datetime=%a-%b-%c-%d-%e-%f
for /F "usebackq tokens=1,2,3,4,5,6,7 delims=-//.:, " %%a IN (`echo %%date%%-%%time%%`) do set my_datetime=%%a%%b%%c%%d%%e%%f
echo %my_datetime%
::for /f "tokens=2" %f in ('tasklist ^| FINDSTR /i /c:"notepad"') do (TASKKILL /PID %f)
for /f "tokens=2" %%f in ('tasklist ^| FINDSTR /i /c:"notepad"') do (TASKKILL /PID %%f)
ping 127.0.0.1 -n 2 > nul
SET originalfolder=C:\Users\%username%\AppData\Roaming\Notepad++
cd "%originalfolder%"
for /f %%f in ('dir /b /s "%originalfolder%\session.xml"') do (
echo echo file: %%~nxf
echo.rename "%%f" %%~nxf.%my_datetime%
rename "%%f" %%~nxf.%my_datetime%
)
::::::::::::::::::::::::::
SET originalfolder=C:\Users\%username%\PortableSoft\Notepad++Portable\App\Notepad++
cd "%originalfolder%"
for /f %%f in ('dir /b /s "%originalfolder%\session.xml"') do (
echo echo file: %%~nxf
echo.rename "%%f" %%~nxf.%my_datetime%
rename "%%f" %%~nxf.%my_datetime%
)
::::::::::::::::::::::::::
cd "C:\Program Files (x86)\Notepad++\"
cd "C:\Users\%username%\PortableSoft\Notepad++Portable\App\Notepad++\"
start notepad++.exe